import axios from 'axios'

const api = {
  requestData (id) {
    return new Promise((resolve, reject) => {
      axios.get('http://47.94.14.118/api/products/detail?id=' + id).then(data => {
        // console.log(data.data.data)
        resolve(data.data.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  addData (url, params) {
    return new Promise((resolve, reject) => {
      axios.post(url, params).then(data => {
        // console.log(data.data.data)
        resolve(data.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  requestGoods (url) {
    return new Promise((resolve, reject) => {
      axios.get(url).then(data => {
        // console.log(data.data.data)
        resolve(data.data.data)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
export default api
