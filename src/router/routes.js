const Home = () => import(/* webpackChunkName: "group-footer" */ '@/pages/Home')
const Kind = () => import(/* webpackChunkName: "group-footer" */ '@/pages/Kind')
const Cart = () => import(/* webpackChunkName: "group-footer" */ '@/pages/Cart')
const User = () => import(/* webpackChunkName: "group-footer" */ '@/pages/User')
const Search = () => import(/* webpackChunkName: "group-search" */ '@/pages/Search')
const Detail = () => import(/* webpackChunkName: "group-detail" */ '@/pages/Detail')
const Footer = () => import(/* webpackChunkName: "group-footer" */ '@/components/Footer')
const City = () => import(/* webpackChunkName: "group-footer" */ '@/pages/City')
const Register = () => import(/* webpackChunkName: "group-footer" */ '@/pages/Register')
const Login = () => import(/* webpackChunkName: "group-footer" */ '@/pages/Login')
const address = () => import(/* webpackChunkName: "group-footer" */ '@/pages/address')

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    components: {
      default: Home,
      footer: Footer
    }
  },
  {
    path: '/cart',
    name: 'cart',
    components: {
      default: Cart
    }
  },
  {
    path: '/kind',
    name: 'kind',
    components: {
      default: Kind,
      footer: Footer
    }
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/user',
    name: 'user',
    components: {
      default: User,
      footer: Footer
    }
  },
  {
    path: '/detail',
    name: 'detail',
    components: {
      default: Detail
    }
  },
  {
    path: '/city',
    name: 'city',
    components: {
      default: City
    }
  },
  {
    path: '/register',
    name: 'register',
    components: {
      default: Register
    }
  },
  {
    path: '/login',
    name: 'login',
    components: {
      default: Login
    }
  },
  {
    path: '/address',
    name: 'address',
    components: {
      default: address
    }
  }
]

export default routes
