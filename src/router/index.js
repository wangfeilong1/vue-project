import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router)

const router = new Router({
  routes
})
router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.path === '/cart') {
    if (JSON.parse(localStorage.getItem('isLogin')).isLogin === 'ok') {
      next()
    } else {
      alert('亲，您还有登录哦!')
      next('/login')
    }
  } else {
    next()
  }
})

export default router
