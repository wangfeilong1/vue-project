import axios from 'axios'

const api = {
  requestSearchData (name) {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=' + name)
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  }
}

export default api
