import axios from 'axios'

const api = {
  requestData (id) {
    // console.log('1111')
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchI?id=' + id)
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  }
}

export default api
