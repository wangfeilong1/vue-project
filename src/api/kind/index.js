import axios from 'axios'

const api = {
  requestLenovoData () {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=联想')
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  },
  requestDellData () {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=戴尔')
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  },
  requestHpData () {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=惠普')
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  },
  requestAsusData () {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=华硕')
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  },
  requestAppleData () {
    return new Promise((resolve, reject) => {
      axios.get('/test/api/product/searchN?name=Apple')
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
  }
}

export default api
